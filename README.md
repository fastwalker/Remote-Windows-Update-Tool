# Remote Windows Update tool

Simple tool for initiating updates on domain-joined servers and Workstations, on both WSUS- and non-WSUS setups. Allows you to select one or multiple OU's and then update them in batches.
Has no requirements other than Powershell and and a functioning WinRM connection.

![img](Docs/screenshot.JPG)
## Usage:

1. Start the tool (client.exe) and enter an AD server and at least 1 OU distinguished name;
2. The tool will show all machines found in the chosen OU's;
3. Select one or more clients in the tool, (de)select 'auto reboot' and click 'Start updates'
4. The selected clients will now receive regular update initiations, meaning the client will continue updating until there are no more updates to install;
5. Pressing 'stop updates' will cause the tool to stop sending update initiations, but it cannot interrupt the actual update process on the remote computer.
