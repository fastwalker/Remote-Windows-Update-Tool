# Standard libs
import os
import sys
import logging
import tkinter as tk


class ConfigUI(tk.Frame):

    def __init__(self, client, *args, **kwargs):
        """
        Config frame for user to control application settings. Shown before opening the main screen.
        """
        super().__init__(*args, **kwargs)
        self.client = client
        self.ad_server = 'ActiveDirectory.example.com'
        self.target_ou = 'CN=servers,DC=example,DC=com; CN=clients,DC=example,DC=com'
        self.initiate_client_updates_interval = 60
        self.default_auto_reboot = False

        try:
            self._load_config_cache()
        except Exception as e:
            logging.log(level=logging.ERROR, msg='Could not read config cache: {0}'.format(e))

    @property
    def target_ous(self):

        return [ou.strip() for ou in self.target_ou.split(';')]

    def on_ad_server_entry_change(self, *args):

        self.ad_server = self.ad_server_value.get()
        self._validate_continue()

    def on_target_ou_entry_change(self, *args):

        self.target_ou = self.target_ou_value.get()
        self._validate_continue()

    def set_default_values(self):

        self.ad_server = 'ActiveDirectory.example.com'
        self.target_ou = 'CN=servers,DC=example,DC=com; CN=clients,DC=example,DC=com'
        self.default_auto_reboot_checkbox.deselect()
        self.initiate_client_updates_interval_scale.set(60)

    def get_values(self):
        """
        Retrieve user values from the various tkinter elements.
        """
        self.ad_server = self.ad_server_value.get().strip()
        self.target_ou = self.target_ou_value.get().strip()
        self.initiate_client_updates_interval = int(self.initiate_client_updates_interval_scale.get() * 1000)
        self.default_auto_reboot = self.default_auto_reboot_checkbox_var.get()

    def start(self):
        """
        Write chosen values and load the main program.
        """
        self.get_values()
        self._write_config_cache()
        self.client.load_clients()
        self.client.load_main_frame()

    def initialize_config_gui(self):

        self.ad_server_value = tk.StringVar(self, value=self.ad_server)
        self.ad_server_value.trace('w', self.on_ad_server_entry_change)
        self.ad_server_label = tk.Label(self, text='AD server (enter a hostname/IP)')
        self.ad_server_entry = tk.Entry(self, textvariable=self.ad_server_value, width=80)

        self.target_ou_value = tk.StringVar(self, value=self.target_ou)
        self.target_ou_value.trace('w', self.on_target_ou_entry_change)
        self.target_ou_label = tk.Label(self, text='Target OUs (list of DN separated by ";")')
        self.target_ou_entry = tk.Entry(self, textvariable=self.target_ou_value, width=80)


        self.initiate_client_updates_interval_scale = tk.Scale(self,
                                                               label='Client update initiation interval (seconds)',
                                                               orient=tk.HORIZONTAL, variable=tk.IntVar, from_=10,
                                                               to=300, tickinterval=30, length=300)
        self.initiate_client_updates_interval_scale.set(self.initiate_client_updates_interval or 60)

        self.default_auto_reboot_checkbox_var = tk.IntVar()
        self.default_auto_reboot_checkbox = tk.Checkbutton(self, text='Default auto reboot',
                                                           variable=self.default_auto_reboot_checkbox_var)
        if self.default_auto_reboot:
            self.default_auto_reboot_checkbox.select()
        else:
            self.default_auto_reboot_checkbox.deselect()

        self.config_start_button = tk.Button(self, text="Start", command=self.start, width=15)
        self.config_exit_button = tk.Button(self, text="Exit", command=self.client.exit, width=15)
        self.config_restore_defaults_button = tk.Button(self, text="Restore defaults", command=self.set_default_values,
                                                        width=15)

        self.config_start_button.grid(row=0, column=0, pady=(10, 10), sticky=tk.W, columnspan=3)
        self.config_exit_button.grid(row=0, column=1, sticky=tk.E, columnspan=3)
        self.ad_server_label.grid(row=1, column=0, pady=(0, 10), sticky=tk.W)
        self.ad_server_entry.grid(row=1, column=1, pady=(0, 10), sticky=tk.W)
        self.target_ou_label.grid(row=2, column=0, pady=(0, 10), sticky=tk.W)
        self.target_ou_entry.grid(row=2, column=1, pady=(0, 10), sticky=tk.W)
        self.default_auto_reboot_checkbox.grid(row=3, pady=(0, 10), sticky=tk.W, columnspan=3)
        self.initiate_client_updates_interval_scale.grid(row=4, pady=(0, 10), sticky=tk.W, columnspan=3)
        self.config_restore_defaults_button.grid(row=5, columnspan=3, pady=(0, 10))

    def _validate_continue(self):
        """
        Main program can only start if an AD server and target OU's have been chosen.
        """
        if self.ad_server_value.get().strip() and self.target_ou_value.get().strip():
            self.config_start_button.configure(state='normal')
        else:
            self.config_start_button.configure(state='disabled')

    def _write_config_cache(self):
        """
        Write the chosen settings to a config_cache file to retrieve later.
        """
        with open('./resources/config_cache', 'w') as ofile:
            ofile.write("{0}\n{1}\n{2}\n{3}\n".format(self.ad_server, self.target_ou,
                                                      int(self.initiate_client_updates_interval / 1000),
                                                      self.default_auto_reboot))

    def _load_config_cache(self):
        """
        Load settings from the config_cache file to retrieve previously used settings.
        """
        if os.path.exists('./resources/config_cache'):
            with open('./resources/config_cache', 'r') as ofile:
                output = ofile.read()
            if output.strip():
                options = ['ad_server', 'target_ou', 'initiate_client_updates_interval', 'default_auto_reboot']
                fields = output.split('\n')
                while fields:
                    setattr(self, options.pop(0), fields.pop(0))

        if self.initiate_client_updates_interval:
            self.initiate_client_updates_interval = int(self.initiate_client_updates_interval)
        if self.default_auto_reboot == '0':
            self.default_auto_reboot = False
        elif self.default_auto_reboot == '1':
            self.default_auto_reboot = True

