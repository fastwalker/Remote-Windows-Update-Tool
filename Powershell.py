# Standard libs
import logging
import subprocess


def send_powershell_command(command, timeout=110):
    """
    Open a shell, start powershell, execute the passed command, return the output.
    :param command (str)
    :return: output (str)
    """
    logging.log(level=logging.INFO, msg='Sending command: {0}'.format(command))
    output = subprocess.check_output(['powershell', '-noninteractive', command+'\n'], shell=True,
                                     timeout=timeout, stdin=subprocess.PIPE, stderr=subprocess.PIPE).decode()
    logging.log(level=logging.DEBUG, msg='Sent command:\n\t "{0}"\nReceived output:\n\t"{1}"'.format(command, output))
    return output


def load_txt(file_name, per_line=False):
    """
    Return txt file as a string.
    :param file_name (str)
    :param per_line: return lines in a list (Bool)
    :return: str or list of strings
    """
    with open(file_name, 'r') as ofile:
        if not per_line:
            return ofile.read().strip()
        else:
            return ofile.readlines()


PSCODE_GET_CLIENTS_FROM_OU = load_txt(file_name='./resources/ps_get_clients_from_ou.txt')
PSCODE_CLIENT_INITIATE_UPDATES = load_txt(file_name='./resources/ps_client_initiate_updates_code.txt')
