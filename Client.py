# Internal libs
import Config
from Powershell import *
# Standard libs
import os
import sys
import logging
import threading
import tkinter as tk
from tkinter.messagebox import showerror


class UpdateUI(tk.Tk):

    def __init__(self, *args, **kwargs):
        """
        A UI that shows all machines in one or more OU's ad allows the user to initiate updates on them.
        The update process can be started for individual or multiple clients, at which point they will be sent a remote
        comand to start updates (optionally including reboots).
        If the automatic reboot option is chosen, the client will keep installing updates and rebooting until there
        are no more updates pending, or until the update process is stopped. If the update process is stopped
        the client will finish installing its' current batch of updates, but will not be sent another update
        initiation.
        """
        super().__init__(*args, **kwargs)
        self._start_logging()

        self.clients = {}
        self.refresh_rate = 250
        self._current_update_thread = threading.Thread()
        self._currently_selected_client = None

        self.config_frame = Config.ConfigUI(client=self)
        self.config_frame.initialize_config_gui()
        self.main_frame = Config.ConfigUI(client=self)
        self._initialize_window()

        # Get config from user before resuming the main program
        self.load_config_frame()
        self.mainloop()

    def load_config_frame(self):
        """
        Load a frame allowing the user to configure application settings. Shown before main program starts.
        """
        self.title('Configuration')
        self.main_frame.grid_remove()
        self.config_frame.grid(row=0, column=0)

    def load_main_frame(self):

        self.title('Remote Windows update tool')
        self.config_frame.grid_remove()
        self.main_frame.grid(row=0, column=0)

    def load_clients(self):
        """
        Get all machines from the target OU'S .
        """
        pop_up = self._create_pop_up_frame(title='Loading...', text='Retrieving clients from specified OUs')
        try:
            self.get_clients_from_ous()
        except subprocess.CalledProcessError:
            self._show_error('Could not retrieve clients from OUs. Please check credentials.')
            self.exit()
        pop_up.destroy()

        logging.log(level=logging.DEBUG, msg='Initializing UI..')
        self._initialize_window()

        # Schedule callbacks for refreshing and managing clients
        self.after(self.refresh_rate, self._refresh_ui)
        self.schedule_next_update_run()

        # Select first available client for initial view
        if self.clients:
            self._select_first_available_client()

    def exit(self, *args):

        for client in self.clients:
            client.management_thread.join()
        self.destroy()
        sys.exit(0)

    def schedule_next_update_run(self, after=None):

        after = after or self.config_frame.initiate_client_updates_interval
        self.after(after, self.update_clients)

    def update_clients(self):
        """
        Initiate updates (if not already running) for each Client that has 'managing_updates' set to True,
        using non-blocking threads.
        """
        self.schedule_next_update_run(self.config_frame.initiate_client_updates_interval)
        logging.log(level=logging.INFO, msg="Initiating updates for each managed client.")
        for wsus_client in self.clients.values():
            if wsus_client.managing_updates:
                self.start_client_update_thread(wsus_client)

    def start_client_update_thread(self, client):
        """
        Initiate updates (if not already running) for the passed client if it has 'managing_updates' set to True,
        using a non-blocking thread.
        """
        logging.log(level=logging.INFO, msg="Initiating updates for client {0} in background thread.".format(
            client.name))
        client.management_thread = \
            threading.Thread(target=self.initiate_client_updates, args=(client,), daemon=True)
        client.management_thread.start()

    def initiate_client_updates(self, client):
        """
        Send the 'invoke-pswindowsupdate' command from the PSWindowsUpdate powershell module to the client. The command
        tells the client to start installing if the windows update service is not busy.
        :param client: Client object
        """
        logging.log(level=logging.INFO, msg="Sending powershell command for initiating updates for client {0}".format(
            client.name))
        if not client.managing_updates:
            return
        try:
            send_powershell_command(PSCODE_CLIENT_INITIATE_UPDATES
                                    .replace("%COMPUTER_NAME%", client.name)
                                    .replace("%AUTO_REBOOT%", "-AutoReboot" if client.auto_reboot else ''),
                                    timeout=self.config_frame.initiate_client_updates_interval - 5)
        except Exception as e:
            client.last_action_was_successful = False
            self._change_client_listbox_color(client=client, color='red')
            logging.log(level=logging.ERROR, msg="Error initiating updates for client {0} with error:\n{1}".format(
                client.name, e))
        else:
            logging.log(level=logging.INFO, msg="Pushed updates to client {0}".format(client.name))
            self._change_client_listbox_color(client=client, color='green')
            client.last_action_was_successful = True

    def get_clients_from_ous(self):
        """
        Get all WSUS groups and add them to an internal dict if their name matches a line in the ./groups.txt file.
        """
        for ou in self.config_frame.target_ous:
            logging.log(level=logging.INFO, msg='Retrieving clients from OU: {0}'.format(ou))
            self.get_clients_from_ou(ou=ou, domain_controller=self.config_frame.ad_server)
        logging.log(level=logging.INFO, msg='Retrieved clients from OUs: {0} using AD server'.format(
            self.config_frame.target_ous, self.config_frame.ad_server))

    def get_clients_from_ou(self, ou, domain_controller):
        """
        Retrieve all clients from the passed DN and domain controller. Specifying a domain controller allows adding
        machines from different domains, i.e. "contoso.com" and "test.contoso.com".
        :param ou: DN of the OU between quotes, e.g.: "OU=servers,OU=company,DC=contoso,DC=com" (str)
        :param domain_controller: FQDN of domain controller that hosts GC of the target domain (str)
        """
        output = send_powershell_command(PSCODE_GET_CLIENTS_FROM_OU.
                                         replace('%TARGET_OU%', ou).replace('%TARGET_SERVER%', domain_controller))
        clients = output.split('\r\n\r\n')
        for client in clients:
            if 'DNSHostName' not in client:
                continue
            name = client.split('\r\nDNSHostName')[1].split(':')[1].split('\n')[0].strip()
            if name and name not in self.clients.keys():
                self.clients[name] = WsusClient(name=name)
        logging.log(level=logging.INFO, msg='Retrieved clients from OU {0} on server {1}'.format(ou, domain_controller))

    def start_updating_client(self, client):

        client.managing_updates = True
        self.start_client_update_thread(client)
        self._refresh_listboxes()
        logging.log(level=logging.INFO, msg='Started managing client: {0}'.format(client.name))

    def _refresh_ui(self):
        """
        Refresh all client information in the UI based on the data in each object in 'self.wsus_clients'.
        """
        self.after(self.refresh_rate, self._refresh_ui)
        self._refresh_ui_client_pane()

    def _refresh_listboxes(self):
        """
        Clear all listboxes and replace each client in the correct one. I.e. unmanaged clients that have updates
        pending go in 'updatable clients', managed clients that are updating go in 'updating clients' and clients
        with no updates pending go in 'finished clients'.
        """
        client_filter = self.clients_filter_value.get().strip() or None
        updating_client_filter = self.updating_clients_filter_value.get().strip() or None

        listbox, selection = self._get_current_selection()
        self._clear_listbox()

        filtered_clients = []
        if client_filter:
            filtered_clients = [client for client in self.clients.values() if
                                client_filter.lower() in client.name.lower()]
        updating_filtered_clients = []
        if updating_client_filter:
            updating_filtered_clients = [client for client in self.clients.values() if
                                         updating_client_filter.lower() in client.name.lower()]

        for client in self._sort_clients():
            if client.last_action_was_successful:
                color = 'green'
            elif client.last_action_was_successful is False:
                color = 'red'
            else:
                color = 'black'

            if not client.managing_updates:
                if client_filter and client not in filtered_clients:
                    continue
                self.clients_listbox.insert(tk.END, client.name)
                self.clients_listbox.itemconfigure(tk.END, fg=color)
            else:
                if updating_client_filter and client not in updating_filtered_clients:
                    continue
                self.updating_clients_listbox.insert(tk.END, client.name)
                self.updating_clients_listbox.itemconfigure(tk.END, fg=color)

        if selection:
            for index in selection:
                listbox.select_set(index)

    def _change_client_listbox_color(self, client, color):

        listbox, index = self._find_index(client)
        listbox.itemconfigure(index, fg=color)

    def _change_client_name(self, client, new_name):

        listbox, index = self._find_index(client)
        if not listbox or not index:
            return
        listbox.delete(index)
        listbox.insert(index, new_name)

    def _find_index(self, client):

        for index, name in enumerate(self.clients_listbox.get(0, tk.END)):
            if name.startswith(client.name):
                return self.clients_listbox, index
        for index, name in enumerate(self.updating_clients_listbox.get(0, tk.END)):
            if name.startswith(client.name):
                return self.updating_clients_listbox, index
        return None, None

    def _get_current_selection(self):
        """
        Find out if one of the listboxes currently has a selected item. If so, return the listbox and the index of
        the selection.
        :return: listbox (TKinter listbox), selection (tuple)
        """
        listbox = None
        selection = None
        if self.clients_listbox.curselection():
            listbox = self.clients_listbox
            selection = self.clients_listbox.curselection()
        elif self.updating_clients_listbox.curselection():
            listbox = self.updating_clients_listbox
            selection = self.updating_clients_listbox.curselection()
        return listbox, selection

    def _clear_listbox(self):
        """
        Clear all items from all listboxes.
        """
        self.clients_listbox.delete(0, tk.END)
        self.updating_clients_listbox.delete(0, tk.END)

    @staticmethod
    def _show_error(warning):
        """
        Display a warning message box.
        :param warning (str)
        """
        showerror('Error', message=warning)

    @staticmethod
    def _create_pop_up_frame(title, text):

        pop_up_frame = tk.Toplevel()
        pop_up_frame.title(title)
        pop_up_frame = tk.Label(pop_up_frame)
        pop_up_frame.grid(row=0, column=0)
        pop_up_frame['text'] = text
        pop_up_frame.update()
        pop_up_frame.lift()
        return pop_up_frame

    def _refresh_ui_client_pane(self):
        """
        Refresh client information for currently selected client in the UI based on the data in each
        object in 'self.wsus_clients'.
        """
        if not self._currently_selected_client:
            return
        self._set_name_label()
        self._set_auto_reboot_checkbox()
        self._set_manage_buttons()

    def _select_first_available_client(self):
        """
        Programmatically select the first available client from the list. Used when initializing the GUI.
        """
        logging.log(level=logging.DEBUG, msg='Initializing default view..')
        self._refresh_listboxes()
        if self.clients_listbox.get(0, tk.END):
            self.clients_listbox.selection_set(0)
            self._select_client(self.clients_listbox)
        elif self.updating_clients_listbox.get(0, tk.END):
            self.updating_clients_listbox.selection_set(0)
            self._select_client(self.updating_clients_listbox)

        self._currently_selected_client = self.clients.values().__iter__().__next__()

    def _select_client(self, listbox):

        if len(listbox.curselection()) == 1:
            client = self.clients[listbox.get(listbox.curselection()[0]).split('(')[0].strip()]
            self._currently_selected_client = client
        elif len(listbox.curselection()) > 1:
            self._currently_selected_client =\
                [self.clients[listbox.get(index).split('(')[0].strip()] for index in listbox.curselection()]
        self._refresh_ui_client_pane()

    def _select_auto_reboot_checkbox(self):

        if isinstance(self._currently_selected_client, list):
            for wsus_client in self._currently_selected_client:
                wsus_client.auto_reboot = self.client_auto_reboot_checkbox_var.get()
        else:
            self._currently_selected_client.auto_reboot = self.client_auto_reboot_checkbox_var.get()

    def _select_install_updates_button(self):

        if isinstance(self._currently_selected_client, list):
            for client in self._currently_selected_client:
                self.start_updating_client(client)
        else:
            self.start_updating_client(self._currently_selected_client)

    def _select_stop_install_updates_button(self):

        if isinstance(self._currently_selected_client, list):
            for wsus_client in self._currently_selected_client:
                wsus_client.managing_updates = False
        else:
            self._currently_selected_client.managing_updates = False
        self._refresh_listboxes()

    def _set_name_label(self):

        if isinstance(self._currently_selected_client, list):
            if self.name_label['text'] != 'Multiple selection':
                self.name_label['text'] = 'Multiple selection'
        else:
            if self._currently_selected_client.name != self.name_label['text']:
                self.name_label['text'] = 'Name: {0}'.format(self._currently_selected_client.name)

    def _set_auto_reboot_checkbox(self):

        if isinstance(self._currently_selected_client, list):
            all_auto_reboot = False not in [client.auto_reboot for client in self._currently_selected_client]
            if all_auto_reboot:
                self.client_auto_reboot_checkbox.select()
            else:
                self.client_auto_reboot_checkbox.deselect()
        else:
            if self._currently_selected_client.auto_reboot != bool(self.client_auto_reboot_checkbox_var.get()):
                if self._currently_selected_client.auto_reboot:
                    self.client_auto_reboot_checkbox.select()
                else:
                    self.client_auto_reboot_checkbox.deselect()

    def _set_manage_buttons(self):

        if isinstance(self._currently_selected_client, list):
            if True not in [client.managing_updates for client in self._currently_selected_client]:
                self.client_install_updates_button['state'] = 'normal'
                self.client_stop_install_updates_button['state'] = 'disabled'
            else:
                self.client_install_updates_button['state'] = 'disabled'
                self.client_stop_install_updates_button['state'] = 'normal'
        else:
            if not self._currently_selected_client.managing_updates:
                self.client_install_updates_button['state'] = 'normal'
                self.client_stop_install_updates_button['state'] = 'disabled'
            else:
                self.client_install_updates_button['state'] = 'disabled'
                self.client_stop_install_updates_button['state'] = 'normal'

    def _on_client_filter_change(self, *args):

        self._refresh_listboxes()

    def _initialize_window(self):

        img = tk.PhotoImage(file= r'./resources/icon.png')
        self.tk.call('wm', 'iconphoto', self._w, img)
        self.title('Remote Windows update tool')

        self.clients_frame = tk.LabelFrame(self.main_frame, text='Computers:')

        self.clients_filter_label = tk.Label(self.clients_frame, text='Name filter:')
        self.clients_filter_value = tk.StringVar(self.clients_frame)
        self.clients_filter_value.trace('w', self._on_client_filter_change)
        self.clients_filter_entry = tk.Entry(self.clients_frame, textvariable=self.clients_filter_value)
        self.clients_listbox = tk.Listbox(self.clients_frame, selectmode='extended', height=15, width=60)
        self.clients_listbox.bind('<<ListboxSelect>>', lambda *args: self._select_client(self.clients_listbox))
        self.clients_listbox_scrollbar = tk.Scrollbar(self.clients_frame)
        self.clients_listbox.config(yscrollcommand=self.clients_listbox_scrollbar.set)
        self.clients_listbox_scrollbar.config(command=self.clients_listbox.yview)

        self.updating_clients_frame = tk.LabelFrame(self.main_frame, text="Updating computers:")
        self.updating_clients_filter_label = tk.Label(self.updating_clients_frame, text='Name filter:')
        self.updating_clients_filter_value = tk.StringVar(self.updating_clients_frame)
        self.updating_clients_filter_value.trace('w', self._on_client_filter_change)
        self.updating_clients_filter_entry = tk.Entry(self.updating_clients_frame,
                                                      textvariable=self.clients_filter_value)
        self.updating_clients_listbox = tk.Listbox(self.updating_clients_frame, selectmode='extended', height=15,
                                                   width=60)
        self.updating_clients_listbox.bind('<<ListboxSelect>>',
                                           lambda *args: self._select_client(self.updating_clients_listbox))
        self.updating_clients_listbox_scrollbar = tk.Scrollbar(self.updating_clients_frame)
        self.updating_clients_listbox.config(yscrollcommand=self.updating_clients_listbox_scrollbar.set)
        self.updating_clients_listbox_scrollbar.config(command=self.updating_clients_listbox.yview)

        self.current_client_frame = tk.LabelFrame(self.main_frame, text='Selected client')
        self.name_label = tk.Label(self.current_client_frame)
        self.client_auto_reboot_checkbox_var = tk.IntVar()
        self.client_auto_reboot_checkbox = tk.Checkbutton(self.current_client_frame, text='Auto reboot',
                                                          variable=self.client_auto_reboot_checkbox_var,
                                                          command=self._select_auto_reboot_checkbox)
        self.client_install_updates_button = tk.Button(self.current_client_frame, text="Start updates",
                                                       command=self._select_install_updates_button)
        self.client_stop_install_updates_button = tk.Button(self.current_client_frame, text="Stop updates",
                                                            command=self._select_stop_install_updates_button)

        self.clients_frame.grid(row=0, column=0, sticky=tk.N+tk.S+tk.W)
        self.clients_listbox.grid(row=0, column=0, sticky=tk.N+tk.S+tk.W, columnspan=5)
        self.clients_listbox_scrollbar.grid(row=0, column=5, sticky=tk.N+tk.S+tk.W)
        self.clients_filter_label.grid(row=1, column=0, sticky=tk.W)
        self.clients_filter_label.configure(anchor=tk.W)
        self.clients_filter_entry.grid(row=1, column=1, sticky=tk.W+tk.E, columnspan=5)

        self.updating_clients_frame.grid(row=1, column=0, sticky=tk.N + tk.S + tk.W)
        self.updating_clients_listbox.grid(row=0, column=0, sticky=tk.N+tk.S+tk.W, columnspan=5)
        self.updating_clients_listbox_scrollbar.grid(row=0, column=5, sticky=tk.N+tk.S+tk.W)
        self.updating_clients_filter_label.grid(row=1, column=0, sticky=tk.W)
        self.updating_clients_filter_label.configure(anchor=tk.W)
        self.updating_clients_filter_entry.grid(row=1, column=1, sticky=tk.W+tk.E, columnspan=5)

        self.current_client_frame.grid(row=0, column=1, sticky=tk.N + tk.S + tk.W, rowspan=2)
        self.name_label.grid(row=0, column=0, sticky=tk.N+tk.S+tk.W, padx=15, pady=(15, 0))
        self.client_auto_reboot_checkbox.grid(row=1, column=0, sticky=tk.N+tk.W+tk.S, padx=15, columnspan=4)
        self.client_install_updates_button.grid(row=2, column=0, sticky=tk.N+tk.W+tk.S, padx=15)
        self.client_install_updates_button['state'] = 'disabled'
        self.client_stop_install_updates_button.grid(row=3, column=0, sticky=tk.N+tk.W+tk.S, padx=15)
        self.client_stop_install_updates_button['state'] = 'disabled'

    def _sort_clients(self):

        return [self.clients[client_name] for client_name in sorted(list(self.clients.keys()))]

    @staticmethod
    def _start_logging():

        if os.path.exists('./client.log'):
            os.remove('./client.log')
        logging.basicConfig(filename='client.log', level=logging.DEBUG,
                            format='[%(asctime)s - %(name)s - %(levelname)s] %(message)s')
        logging.log(level=logging.INFO, msg='Initializing client..')


class WsusClient:

    def __init__(self, name, auto_reboot=False, managing_updates=False):
        """
        Represents a WSUS client. Name, id and available updates are pulled from WSUS API. Service status is determined
        by checking for update processes on the remote client (using invoke-command). The log file is pulled
        remotely from the client by reading c:\PSWindowsupdate.log.
        :param name (str)
        """
        self.name = name

        self.management_thread = threading.Thread()

        self.auto_reboot = auto_reboot
        self.managing_updates = managing_updates
        self.last_action_was_successful = None


if __name__ == '__main__':

    ui = UpdateUI()
